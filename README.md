# This GitLab link
https://gitlab.com/gkbuza/slides

## Description
Collection of slides.

## Contributing
Corrections are much welcomed. Letting me know about typos would be much appreciated. Telling me what you consider worth saying would be much appreciated too.

## Contact
gkbnotes@gmail.com
